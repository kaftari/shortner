# Project Title

A nice shortner link with slim 3

---
## Requirements

For development, you will only need PHP 7.4 and MYSQL 8.0 and  Composer 2 for install in your dependency.

## Install

    $ git clone https://gitlab.com/kaftari/shortner.git
    $ cd PROJECT_TITLE
    $ composer install
    $ replace enviroment variable to .env file

## Configure app

Open `.env` then edit it with your settings. You will need:

- Database Connection;
- MemCached Connection;
- JWT Key;

## Running the project

    $ php -S localhost:8000 -t public/