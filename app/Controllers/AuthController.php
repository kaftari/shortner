<?php

namespace App\Controllers;

use App\Models\User;
use App\Repository\User\UserRepository;
use App\Requests\CustomRequestHandler;
use App\Response\CustomResponse;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\RequestInterface as Request;
use App\Validation\Validator;

class AuthController
{
    protected $customResponse;
    protected $user;
    protected $validator;

    public function __construct()
    {
        $this->customResponse = new CustomResponse();
        $this->user = new UserRepository();
        $this->validator = new Validator();
    }

    public function register(Request $request, Response $response)
    {
        $this->validator->validate($request, $this->user->rules());

        if ($this->validator->failed()) {
            $responseMessage = $this->validator->errors;
            return $this->customResponse->is400BadRequest($response, $responseMessage);
        }

        $data = [
            'username' => CustomRequestHandler::getParam($request, "username"),
            'password' => CustomRequestHandler::getParam($request, "password"),
        ];

        $this->user->create($data);
        return $this->customResponse->success($response, 'User created...');
    }

    public function login(Request $request, Response $response)
    {
        $this->validator->validate($request, $this->user->rules());

        if ($this->validator->failed()) {
            $responseMessage = $this->validator->errors;
            return $this->customResponse->is400BadRequest($response, $responseMessage);
        }

        $data = [
            'username' => CustomRequestHandler::getParam($request, "username"),
            'password' => CustomRequestHandler::getParam($request, "password"),
        ];

        if (!$this->user->checkUserExist($data)) {
            return $this->customResponse->is404NotFound($response, 'User not found!');
        }

        $responseMessage = [
            'access-token' => GenerateTokenController::generateToken($data['username'])
        ];
        return $this->customResponse->success($response, $responseMessage);

    }

}