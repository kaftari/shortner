<?php

namespace App\Controllers;

use Firebase\JWT\JWT;

class GenerateTokenController
{
    public static function generateToken($username): string
    {
        $now = time();
        $future = strtotime('+1 hour',$now);
        $secret = env('JWT_SECRET_KEY');

        $payload = [
            "jti" => $username,
            "iat" => $now,
            "exp" => $future
        ];

        return JWT::encode($payload,$secret,"HS256");
    }
}