<?php

namespace App\Controllers;

use App\Repository\Link\LinkRepository;
use App\Requests\CustomRequestHandler;
use App\Response\CustomResponse;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\RequestInterface as Request;
use App\Validation\Validator;


class LinkController
{
    protected $customResponse;
    protected $link;
    protected $validator;

    public function __construct()
    {
        $this->customResponse = new CustomResponse();
        $this->link = new LinkRepository();
        $this->validator = new Validator();
    }

    public function create(Request $request, Response $response)
    {
        $this->validator->validate($request, $this->link->rules());

        if ($this->validator->failed()) {
            $responseMessage = $this->validator->errors;
            return $this->customResponse->is400BadRequest($response, $responseMessage);
        }

        $long_url = CustomRequestHandler::getParam($request, "long_url");

        if ($data = $this->link->getLinkByLongUrl($long_url)) {
            return $this->customResponse
                ->success($response,
                    [
                        'message' => 'url exist in database',
                        'data' => [
                            'long_url' => $data->long_url,
                            'short_code' => env('APP_URL') . $data->short_code]
                        ]
                );
        }

        $responseMessage = $this->link->insertLink([
            "long_url" => $long_url,
        ]);

        return $this->customResponse->success($response, $responseMessage);

    }

    public function edit(Request $request, Response $response, $params)
    {
        try {

            $this->validator->validate($request, $this->link->rules());

            if ($this->validator->failed()) {
                $responseMessage = $this->validator->errors;
                return $this->customResponse->is400BadRequest($response, $responseMessage);
            }

            $long_url = CustomRequestHandler::getParam($request, "long_url");
            $responseMessage = $this->link->editLink(['id' => $params['id'], 'long_url' => $long_url]);
            if (!$responseMessage || !count($responseMessage))
                return $this->customResponse->is404NotFound($response, 'Link not found!');

            return $this->customResponse->success($response, $responseMessage);
        } catch (\Exception $exception) {
            return $this->customResponse->is500InternalError($response, $exception->getMessage());
        }
    }

    public function delete(Request $request, Response $response, $params)
    {
        if ($this->link->deletLink($params['id'])) {
            $responseMessage = "Link Deleted!";
            return $this->customResponse->success($response, $responseMessage);
        }

        $responseMessage = "Oops! Something went wrong.";
        return $this->customResponse->is400BadRequest($response, $responseMessage);

    }

    public function index(Request $request, Response $response, $params)
    {
        $entity = $this->link->getLinkByShortCode($params['short_code']);

        if (!$entity)
            return $this->customResponse->is404NotFound($response, 'Url not found!');

        return $response->withRedirect($entity->long_url);
    }
}