<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table="links";

    protected $fillable = ["long_url","short_url","created_at"];
}