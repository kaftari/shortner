<?php

namespace App\Repository\Link;

use App\Models\Link;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Support\Facades\Date;
use Respect\Validation\Validator as Validate;

class LinkRepository extends Link
{
    public function insertLink(array $data)
    {
        try {

            DB::beginTransaction();

            DB::table('links')->insert([
                "long_url" => $data['long_url'],
                "created_at" => Date::now(),
                "updated_at" => Date::now(),
            ]);

            $lastInsertId = DB::getPdo()->lastInsertId();

            $row = tap(DB::table('links')
                ->where('id', $lastInsertId))
                ->update([
                    'short_code' => $this->generateShortLink($lastInsertId)
                ])
                ->get()
                ->map(function ($item) {
                    return [
                        'long_url' => $item->long_url,
                        'short_code' => env('APP_URL') . $item->short_code
                    ];
                });

            DB::commit();

            return $row;

        } catch (\Exception $exception) {
            DB::rollBack();
            self::throwBadMethodCallException($exception->getMessage());
        }
    }

    public function editLink($data)
    {
        return tap(DB::table('links')->where('id', $data['id']))
            ->update([
                'long_url' => $data['long_url']
            ])
            ->get()
            ->map(function ($item) {
                return [
                    'long_url' => $item->long_url,
                    'short_code' => env('APP_URL') . $item->short_code
                ];
            });
    }

    public function deletLink($data): int
    {
        return DB::table('links')->delete($data['id']);
    }

    protected function generateShortLink($lastIdGenerate): string
    {
        return uniqid() . $lastIdGenerate;
    }

    public function getLinkByLongUrl($url)
    {
        return DB::table('links')->where('long_url', $url)->first();
    }

    public function getLinkByShortCode($shortCode)
    {
        return DB::table('links')->where('short_code', $shortCode)->first();
    }

    public function rules(): array
    {
        return [
            'long_url' => Validate::notEmpty()->url(),
        ];
    }
}