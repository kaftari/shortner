<?php

namespace App\Repository\User;

use App\Models\User;
use Illuminate\Database\Capsule\Manager as DB;
use Respect\Validation\Validator as Validate;

class UserRepository extends User
{
    public function create(array $data)
    {
        try {

            DB::table('users')
                ->insert([
                    "username" => $data['username'],
                    "password" => $this->hashPassword($data['password']),
                ]);

        } catch (\Exception $exception) {
            self::throwBadMethodCallException($exception->getMessage());
        }
    }

    public function checkUserExist($data)
    {
        $user =  DB::table('users')
            ->where('username', $data['username'])
            ->first();

        if (!$user) {
            return false;
        }

        if (!password_verify($data['password'], $user->password))
            return false;

        return true;
    }

    public function hashPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function rules(): array
    {
        return [
            'username' => Validate::notEmpty(),
            'password' => Validate::notEmpty(),
        ];
    }
}