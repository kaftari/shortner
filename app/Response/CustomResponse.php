<?php

namespace App\Response;

class CustomResponse
{
    public function success($response, $responseMessage)
    {
        $responseMessage = json_encode(['success'=>true, 'response'=> $responseMessage]);
        $response->getBody()->write($responseMessage);
        return $response->withHeader("Content-Type", "application/json")
            ->withStatus(200);
    }

    public function is500InternalError($response, $responseMessage)
    {
        $responseMessage = json_encode(['success'=>false, 'response'=> $responseMessage]);
        $response->getBody()->write($responseMessage);
        return $response->withHeader("Content-Type", "application/json")
            ->withStatus(500);
    }

    public function is400BadRequest($response, $responseMessage)
    {
        $responseMessage = json_encode(['success'=>false, 'response'=> $responseMessage]);
        $response->getBody()->write($responseMessage);
        return $response->withHeader("Content-Type", "application/json")
            ->withStatus(400);
    }

    public function is422BadRequest($response, $responseMessage)
    {
        $responseMessage = json_encode(['success'=>false, 'response'=> $responseMessage]);
        $response->getBody()->write($responseMessage);
        return $response->withHeader("Content-Type", "application/json")
            ->withStatus(422);
    }

    public function is404NotFound($response, $responseMessage)
    {
        $responseMessage = json_encode(['success'=>false, 'response'=> $responseMessage]);
        $response->getBody()->write($responseMessage);
        return $response->withHeader("Content-Type", "application/json")
            ->withStatus(404);
    }
}