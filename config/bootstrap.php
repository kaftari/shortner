<?php

use Slim\App;
use Dotenv\Dotenv;

require_once __DIR__ . '/../vendor/autoload.php';
$settings =  require_once __DIR__ . '/settings.php';
$app = new App($settings);
$dotenv = Dotenv::createImmutable(__DIR__. '/../');
$dotenv->load();


$container = $app->getContainer();
require_once __DIR__ . '/errorHandler.php';

$routeContainers = require_once __DIR__ . '/routeContainers.php';
$routeContainers($container);

require_once __DIR__ . '/routes.php';
require_once __DIR__ . '/database.php';

$middleware = require_once __DIR__ . "/middleware.php";
$middleware($app);

$app->run();