<?php
$database_config =[
    "driver"=>"mysql",
    "host"=>env('DB_HOST'),
    "database"=>env('DB_NAME'),
    "username"=>env('DB_USERNAME'),
    "password"=>env('DB_PASSWORD'),
    "charset"=>"utf8",
    "collation"=>"utf8_general_ci",
    "prefix"=>""
];

$capsule =new \Illuminate\Database\Capsule\Manager;

$capsule->addConnection($database_config);

$capsule->setAsGlobal();

$capsule->bootEloquent();

return $capsule;