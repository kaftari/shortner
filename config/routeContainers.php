<?php

use App\Controllers\LinkController;
use App\Controllers\AuthController;

return function ($container) {
    $container["LinkController"] = function () {
        return new LinkController();
    };
    $container["AuthController"] = function () {
        return new AuthController();
    };
};