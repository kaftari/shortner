<?php

$app->patch("/edit-link/{id}", "LinkController:edit");
$app->post("/create-link", "LinkController:create");
$app->delete("/delete-link/{id}", "LinkController:delete");
$app->get("/list-links", "LinkController:list");
$app->get("/{short_code}", "LinkController:index");
$app->get("/", function ($req, $res){
    return $res->withJson(['success' => true, 'message' => 'Welcome dear...']);
});

$app->group("/auth", function () use ($app) {
   $app->post("/login", "AuthController:login");
   $app->post("/register", "AuthController:register");
});